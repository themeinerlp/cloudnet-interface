const pages = require("../script/pages");
const socket = require("../script/socket");
const cloudNetModule = require("../script/cloudnet");
const loginModule = require("../../page/loginStage/login");
const Storage = require("../../storage");
const storage = new Storage("userData", "credentials.json");

const closeListener = () => {
    pages.showPage("login");

    M.toast({html: "Die Authentifizierung war nicht erfolgreich. Module installiert und Cloud gestartet?"});
    console.log("Auth not successful.");
    loginModule.invalidateInputs();
};
socket.registerListener("close", closeListener);

document.addEventListener("DOMContentLoaded", async () => {
    pages.loadStagePages("login");
    storage.loadData();
    if(storage.has("address") && storage.has("authToken")) {
        pages.showPage("loading");
        await tokenLogin(storage.get("address"), storage.get("authToken"));
    } else {
        pages.showPage("login");
    }
});

async function login(address, user, password) {
    const results = await basicLogin(address, "auth", user, password);
    const authToken = results[0];
    const moduleVersion = results[1];

    if(moduleVersion !== process.env.npm_package_version) {
        M.toast({html: "Die Version des Interfaces entspricht nicht der des Interfaces"});
        console.log("Interface-Version: " + process.env.npm_package_version);
        console.log("Module-Version: " + moduleVersion);
        socket.getWebSocket().close();
    }

    storage.put("address", address);
    storage.put("authToken", authToken);
    storage.saveData();

    await handleSuccessfulLogin();
}

async function tokenLogin(address, authToken) {
    const moduleVersion = await basicLogin(address, "auth-token", authToken);

    if(moduleVersion !== process.env.npm_package_version) {
        M.toast({html: "Die Version des Interfaces entspricht nicht der des Interfaces"});
        console.log("Interface-Version: " + process.env.npm_package_version);
        console.log("Module-Version: " + moduleVersion);
        socket.getWebSocket().close();
    }
    await handleSuccessfulLogin();
}

async function basicLogin(address, message, ...values) {
    if(!address.toLowerCase().startsWith("ws://"))
        address = "ws://" + address;
    await socket.openConnection(address);
    return await socket.sendSocketQuery0(message, values);
}

async function handleSuccessfulLogin() {
    pages.showPage("loading");
    setTimeout(async () => {
        socket.removeListener("close", closeListener);

        const cloudNet = new cloudNetModule.CloudNet();
        cloudNetModule.setCloudNet(cloudNet);
        console.log("Auth successful!");

        if(!pages.areInterfacePagesLoaded()) {
            pages.loadStagePages("interface");
            pages.showPage("home");
            await pages.loadInterfacePageScripts();
        } else
            pages.showPage("home");
    }, 1500);
}




module.exports.login = login;