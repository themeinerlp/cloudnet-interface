const domCache = {};

(async () => {
    document.getElementById("refresher").addEventListener("click", updateHomePage, false);

    await updateHomePage();
})();


async function updateHomePage() {
    const cloudNet = require("../../../main/script/cloudnet").getCloudNet();

    // Single cards
    updateElementById("onlineCount", cloudNet.onlineCount);
    updateElementById("serverCount", Object.keys(cloudNet.serverInfos).length);
    updateElementById("proxyCount", Object.keys(cloudNet.proxyInfos).length);
    updateElementById("wrapperCount", cloudNet.wrappers.length);
    updateElementById("memory", cloudNet.usedMemory + "/" + cloudNet.maxMemory + " MB");
    updateElementById("cpuUsage", cloudNet.cpuUsage + "%");

    // Statistics
    updateElementById("registeredPlayers", cloudNet.registeredPlayers);
    updateElementById("playerLogin", cloudNet.statistics["playerLogin"]);
    updateElementById("highestPlayerOnline", cloudNet.statistics["highestPlayerOnline"]);
    updateElementById("playerCommandExecutions", cloudNet.statistics["playerCommandExecutions"]);
    updateElementById("wrapperConnections", cloudNet.statistics["wrapperConnections"]);
    updateElementById("cloudStartup", cloudNet.statistics["cloudStartup"]);
    updateElementById("startedServers", cloudNet.statistics["startedServers"]);
    updateElementById("startedProxys", cloudNet.statistics["startedProxys"]);
    updateElementById("highestServerOnlineCount", cloudNet.statistics["highestServerOnlineCount"]);

    const timeInHours = ((cloudNet.statistics["cloudOnlineTime"] / 3600000) % 24).toFixed(1);
    updateElementById("cloudOnlineTime", timeInHours + " Stunden");

}

/**
 * Updates an element and caches it, if it's not in the cache already
 *
 * @param id id of the element
 * @param value value that should be replaced in the innerText
 */

function updateElementById(id, value) {
    let element;
    if(domCache.hasOwnProperty(id))
        element = domCache[id];
    else {
        element = document.getElementById(id);
        domCache[id] = element;
    }

    element.innerText = element.innerText.replace(element.innerText.split(": ")[1], value);
}
